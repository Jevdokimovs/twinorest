Rest services test.

HOST/loans/ (GET) - list of loans
HOST/loans/{id} (GET) - loan by id
HOST/loans/{id} (POST) - add loan
HOST/loans/{id} (DELETE) - delete loan by id
HOST/loans/user/{personalId} (GET) - list of loans by user personal id 
HOST - web test

TMP hosted on Red Hat OpenShift production
http://jbossews-twinotest.rhcloud.com/loans/ (GET)
http://jbossews-twinotest.rhcloud.com/loans/{id} (GET)
http://jbossews-twinotest.rhcloud.com/loans/{id} (POST)
http://jbossews-twinotest.rhcloud.com/loans/{id} DELETE
http://jbossews-twinotest.rhcloud.com/loans/user/{personalId} (GET)
http://jbossews-twinotest.rhcloud.com - web test

Java7, MySQL db.