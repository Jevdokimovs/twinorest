package twinorest.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import twinorest.entity.Loan;
import twinorest.service.LoanService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:WEB-INF/spring-app-dispatcher.xml"})
@WebAppConfiguration
public class LoanControllerTest {
    private Logger log = Logger.getLogger(LoanControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    private LoanService loanService;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.loanService = new LoanService();
    }

    @Test
    public void loansList() throws Exception {

        log.trace("Get full list, REST path: " + LoanRestURIConstants.GET_ALL_LOANS);

        MvcResult result = mockMvc.perform(get(LoanRestURIConstants.GET_ALL_LOANS).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", equalTo(true)))
                .andExpect(jsonPath("$.statusCode", equalTo(LoanRestResponse.STATUS_OK)))
                .andExpect(jsonPath("$.data", notNullValue()))
                .andExpect(jsonPath("$.data").isArray())
                        //.andDo(print())
                .andReturn();

        String responseJSON = result.getResponse().getContentAsString();
        log.trace("responseJSON: " + responseJSON);
    }

    @Test
    public void getLoanById() throws Exception {
        List<Integer> validResponseStatusList = new ArrayList<Integer>() {{
            add(LoanRestResponse.STATUS_OK);
            add(LoanRestResponse.LOAN_NOT_EXISTS);
        }};

        int testEntityId = 29;
        String restPath = LoanRestURIConstants.GET_LOAN;
        log.trace("Get loan by (ID): " + testEntityId + ", REST path: " + restPath);
        log.trace("validResponseStatusList: " + validResponseStatusList.toString());

        MvcResult result = mockMvc.perform(delete(restPath, testEntityId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode", isIn(validResponseStatusList)))
                        //.andDo(print())
                .andReturn();

        String responseJSON = result.getResponse().getContentAsString();
        log.trace("responseJSON: " + responseJSON);
    }

    @Test
    public void listLoansByUser() throws Exception {
        List<Integer> validResponseStatusList = new ArrayList<Integer>() {{
            add(LoanRestResponse.STATUS_OK);
            add(LoanRestResponse.LOAN_NOT_EXISTS);
        }};

        String testUserPersonalId = "AB123123";
        String restPath = LoanRestURIConstants.GET_USER_LOANS;
        log.trace("Get loan of user with (PersonalID): " + testUserPersonalId + ", REST path: " + restPath);
        log.trace("validResponseStatusList: " + validResponseStatusList.toString());

        MvcResult result =  mockMvc.perform(get(restPath, testUserPersonalId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode", isIn(validResponseStatusList)))
                .andExpect(jsonPath("$.data", notNullValue()))
                .andExpect(jsonPath("$.data").isArray())
                        //.andDo(print())
                .andReturn();

        log.trace("responseJSON: " + result.getResponse().getContentAsString());
    }

    @Test
    public void addLoan() throws Exception {
        List<Integer> validResponseStatusList = new ArrayList<Integer>() {{
            add(LoanRestResponse.STATUS_OK);
            add(LoanRestResponse.REJECTION_APPLICATIONS_LIMIT);
            add(LoanRestResponse.REJECTION_PERSONAL_ID_BLACKLISTED);
        }};

        Loan testEntity = new Loan();
        testEntity.setAmount(new BigDecimal("100"));
        testEntity.setTerm(36);
        testEntity.setCountryCode("lv");
        testEntity.setFirstName("Firstname-test1");
        testEntity.setLastName("Lastname-test1");
        testEntity.setPersonalId("pk-tes1");

        log.trace("Add loan: " + testEntity.toString() + ", REST path: " + LoanRestURIConstants.CREATE_LOAN);
        log.trace("validResponseStatusList: " + validResponseStatusList.toString());

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValueAsString(testEntity);

        MvcResult result = mockMvc.perform(post(LoanRestURIConstants.CREATE_LOAN)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(testEntity))
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode", isIn(validResponseStatusList)))
                        //.andDo(print())
                .andReturn();

        log.trace("responseJSON: " + result.getResponse().getContentAsString());
    }

    @Test
    public void addLoanBlacklisted() throws Exception {
        String testBlacklistedPersonalId = "241275-35865";
        List<Integer> validResponseStatusList = new ArrayList<Integer>() {{
            add(LoanRestResponse.REJECTION_APPLICATIONS_LIMIT); //we also can get application limit
            add(LoanRestResponse.REJECTION_PERSONAL_ID_BLACKLISTED);
        }};

        Loan testEntity = new Loan();
        testEntity.setAmount(new BigDecimal("100"));
        testEntity.setTerm(12);
        testEntity.setFirstName("Firstname-test");
        testEntity.setLastName("Lastname-test");
        testEntity.setPersonalId(testBlacklistedPersonalId);

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValueAsString(testEntity);

        log.trace("Add blacklisted loan(testBlacklistedPersonalId: testBlacklistedPersonalId): " + testEntity.toString() + ", REST path: " + LoanRestURIConstants.CREATE_LOAN);
        log.trace("validResponseStatusList: " + validResponseStatusList.toString());

        MvcResult result =  mockMvc.perform(post(LoanRestURIConstants.CREATE_LOAN)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(testEntity))
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode", isIn(validResponseStatusList)))
                        //.andDo(print())
                .andReturn();

        log.trace("responseJSON: " + result.getResponse().getContentAsString());
    }


    @Test
    public void addLoanLimitTest() throws Exception {
        int validResponseStatusCode = LoanRestResponse.REJECTION_APPLICATIONS_LIMIT;

        Loan testEntity = new Loan();
        testEntity.setAmount(new BigDecimal("100"));
        testEntity.setTerm(12);
        testEntity.setCountryCode("lv"); //take default lv code

        log.trace("Add out of limit loan(limit: "+LoanService.DEFAULT_REJECTION_LOANS_COUNT+"), REST path: " + LoanRestURIConstants.CREATE_LOAN);
        log.trace("validResponseStatusList: " + validResponseStatusCode);

        //add n loans
        for(int i=1; i<=LoanService.DEFAULT_REJECTION_LOANS_COUNT; i++) {
            testEntity.setFirstName("Firstname-test"+i);
            testEntity.setLastName("Lastname-test" + i);
            testEntity.setPersonalId("pk-test" + i);
            log.trace("fill DB, add test entity: "+testEntity.toString());

            loanService.insert(testEntity);
        }

        testEntity.setFirstName("Firstname-test"+(LoanService.DEFAULT_REJECTION_LOANS_COUNT+1));
        testEntity.setLastName("Lastname-test" + (LoanService.DEFAULT_REJECTION_LOANS_COUNT+1));
        testEntity.setPersonalId("pk-test" + (LoanService.DEFAULT_REJECTION_LOANS_COUNT+1));

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValueAsString(testEntity);

        log.trace("try add entity: "+testEntity.toString());

        MvcResult result =  mockMvc.perform(post(LoanRestURIConstants.CREATE_LOAN)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(testEntity))
        )
                        .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode", equalTo(validResponseStatusCode)))
                        //.andDo(print())
                .andReturn();

        log.trace("responseJSON: " + result.getResponse().getContentAsString());
    }

    @Test
    public void deleteLoan() throws Exception {
        List<Integer> validResponseStatusList = new ArrayList<Integer>() {{
            add(LoanRestResponse.STATUS_OK);
            add(LoanRestResponse.LOAN_NOT_EXISTS);
        }};

        int testEntityId = 29;
        String restPath = LoanRestURIConstants.DELETE_LOAN;
        log.trace("Delete loan(ID): " + testEntityId + ", REST path: " + restPath);

        MvcResult result = mockMvc.perform(delete(restPath, testEntityId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode", isIn(validResponseStatusList)))
                        //.andDo(print())
                .andReturn();

        log.trace("responseJSON: " + result.getResponse().getContentAsString());
    }
}
