package twinorest.service;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import twinorest.entity.Loan;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:WEB-INF/spring-app-dispatcher.xml"})
public class LoanServiceTest extends TestCase {
    private Logger log = Logger.getLogger(LoanServiceTest.class);
    private LoanService loanService;

    @Before
    public void setUp() {
        this.loanService = new LoanService();
    }

    @Test
    public void getSortedFullList() throws Exception {
        log.trace("Check get full filtered list of loans");

        List<Loan> testList;
        assertNotNull(testList = loanService.getSortedFullList());
        assertThat(testList, instanceOf(List.class));
    }

    @Test
    public void getSortedUserList() throws Exception {
        String testUserPersonalId = "250195-34562";
        log.trace("Check get list of loans by user personal ID: "+testUserPersonalId);

        List<Loan> testList;
        assertNotNull(testList = loanService.getSortedUserList(testUserPersonalId));
        assertThat(testList, instanceOf(List.class));
    }

    @Test
    public void getLoansOfCountryList() throws Exception {
        String testCountryCode = "lv";
        int testRejectTimeFrameDays = LoanService.DEFAULT_REJECTION_TIME_FRAME_DAYS;
        log.trace("Check get list of country code: "+testCountryCode+", rejected in time frame: "+testRejectTimeFrameDays+" days");

        List<Loan> testList;
        assertNotNull(testList = loanService.getLoansOfCountryList(testCountryCode, testRejectTimeFrameDays));
        assertThat(testList, instanceOf(List.class));
    }

    @Test
    public void insert() throws Exception {
        Loan testEntity = new Loan();
        testEntity.setAmount(new BigDecimal("45000.50"));
        testEntity.setTerm(24);
        testEntity.setCountryCode("lv");
        testEntity.setFirstName("Jānis");
        testEntity.setLastName("Liepiņš");
        testEntity.setPersonalId("300491-45212");

        log.trace("Check add testEntity: "+testEntity.toString());

        assertNotNull(testEntity = loanService.insert(testEntity));
        assertThat(testEntity, instanceOf(Loan.class));
        assertThat(testEntity.getId(), greaterThanOrEqualTo(0)); //id generated
        assertThat(testEntity.getApplyDate(), instanceOf(java.sql.Timestamp.class)); //apply date generated

        log.trace("delete testEntity: "+testEntity.toString());
        loanService.delete(testEntity);
    }
}