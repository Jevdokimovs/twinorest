package twinorest.service;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:WEB-INF/spring-app-dispatcher.xml"})
public class BlackListItemServiceTest extends TestCase {
    private Logger log = Logger.getLogger(BlackListItemServiceTest.class);
    private BlackListItemService blackListItemService;

    @Before
    public void setUp() {
        this.blackListItemService = new BlackListItemService();
    }

    @Test
    public void isBlackListed() throws Exception {
        String blackListedPersonalID = "030275-43352";
        String notBlackListedPersonalID = "301181-10325";

        log.trace("Check blacklisted personal ID: "+blackListedPersonalID);
        assertEquals(blackListItemService.isBlackListed(blackListedPersonalID), true);

        log.trace("Check not blacklisted personal ID: "+notBlackListedPersonalID);
        assertEquals(blackListItemService.isBlackListed(notBlackListedPersonalID), false);
    }
}