package twinorest.service.helper;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:WEB-INF/spring-app-dispatcher.xml"})
public class GeoIPCheckerTest  extends TestCase {
    private Logger log = Logger.getLogger(GeoIPCheckerTest.class);

    private GeoIPChecker geoIPChecker;
    @Value(value = "classpath:GeoLiteCity.dat")
    private Resource rGeoIPdb;

    @Before
    public void setUp() {
        this.geoIPChecker = new GeoIPChecker();
    }

    @Test
    public void getCountryCode() throws Exception {
        Map<String, String> validIpCountryRel = new HashMap<String, String>() {{
            put("166.70.157.58", "us");
            put("5.105.112.144", "ua");
            put("176.118.241.74", "ru");
            put("52.64.66.93", "au");
            put("195.72.102.137", "de");
            put("188.213.168.239", "it");
        }};

        log.trace("GEO IP test for valid " + validIpCountryRel.size() + " entries");
        for (Map.Entry<String, String> entry : validIpCountryRel.entrySet()) {
            log.trace("check IP " + entry.getKey() + " is : " + entry.getValue());
            assertEquals(geoIPChecker.getCountryCode(entry.getKey(), rGeoIPdb), entry.getValue());
        }
    }
}