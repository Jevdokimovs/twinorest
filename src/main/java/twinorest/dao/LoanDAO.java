package twinorest.dao;

import twinorest.entity.Loan;

import java.util.List;

public interface LoanDAO {
    Loan insert(Loan entity);
    Loan getById(Integer id);
    @SuppressWarnings("unused")
    List<Loan> list();
    boolean delete(Loan entity);
}
