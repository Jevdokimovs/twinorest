package twinorest.dao;


import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import twinorest.dao.util.HibernateUtil;
import twinorest.entity.BlackListItem;

public abstract class BlackListItemDAOImpl implements BlackListItemDAO {
    private Logger log = Logger.getLogger(BlackListItemDAO.class);

    public BlackListItemDAOImpl() {}

    @SuppressWarnings("unchecked")
    public BlackListItem getByPersonalId(String personalId) {
        log.trace("Getting entity by personalId: "+personalId);

        @SuppressWarnings("unchecked")
        BlackListItem entity = null;
        Session session = this.getSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            entity  = (BlackListItem)session.createCriteria(BlackListItem.class).add(Restrictions.eq("personalId", personalId)).uniqueResult();

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            log.error("Can't get entity list", e);
            return null;
        }finally {
            session.close();
        }

        log.trace("done");
        return entity;
    }

    protected Session getSession() {
        return HibernateUtil.getInstance().getSession();
    }
}
