package twinorest.dao.util;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    @SuppressWarnings("FieldCanBeLocal")
    private Logger log = Logger.getLogger(HibernateUtil.class);

    private final static String HIBERNATE_CFG_FILE = "/hibernate.cfg.xml";
    private static HibernateUtil instance = null;

    private static SessionFactory sessionFactory;

    private HibernateUtil(){
        log.trace("Init hibernate configuration, cfg file: " + HIBERNATE_CFG_FILE);

        Configuration configuration = new Configuration().configure(HIBERNATE_CFG_FILE);

        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        log.trace("Init hibernate config done");
    }

    public static synchronized HibernateUtil getInstance(){
        if(instance == null){
            instance  = new HibernateUtil();
        }
        return instance;
    }

    @SuppressWarnings("unused")
    public synchronized SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public synchronized Session getSession() throws HibernateException {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}


