package twinorest.dao;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import twinorest.dao.util.HibernateUtil;
import twinorest.entity.Loan;

import java.util.List;

public abstract class LoanDAOImpl implements LoanDAO {
    private Logger log = Logger.getLogger(LoanDAOImpl.class);

    public LoanDAOImpl() {}

    @Override
    public Loan insert(Loan entity) {
        log.trace("Inserting entity: "+entity.toString());

        Session session = this.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(entity);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            log.error("Can't insert entity", e);
            return null;
        } finally {
            session.close();
        }

        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Loan> list() {
        log.trace("Getting full list of entity");

        @SuppressWarnings("unchecked")
        List<Loan> list = null;
        Session session = this.getSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            list = session.createQuery("FROM Loan").list();

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            log.error("Can't get entity list", e);
        }finally {
            session.close();
        }

        log.trace("done");
        return list;
    }

    @Override
    public Loan getById(Integer id) {
        log.trace("Getting entity by id: "+id);
        Loan entity = null;

        Session session = this.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            entity = (Loan)session.get(Loan.class, id);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            log.error("Can't get entity by ID: "+id, e);
        } finally {
            session.close();
        }

        return entity;
    }

    public boolean delete(Loan entity) {
        log.trace("Deleting entity: "+entity.toString());

        Session session = this.getSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            session.delete(entity);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            log.error("Can't delete entity", e);
            return false;
        }finally {
            session.close();
        }

        log.trace("done");

        return true;
    }

    protected Session getSession() {
        return HibernateUtil.getInstance().getSession();
    }
}
