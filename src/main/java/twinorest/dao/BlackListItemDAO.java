package twinorest.dao;

import twinorest.entity.BlackListItem;

public interface BlackListItemDAO {
    BlackListItem getByPersonalId(String personalId);
}
