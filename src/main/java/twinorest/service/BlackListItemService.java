package twinorest.service;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import twinorest.dao.BlackListItemDAOImpl;
import twinorest.entity.BlackListItem;

import java.util.ArrayList;
import java.util.List;

public class BlackListItemService extends BlackListItemDAOImpl {
    private Logger log = Logger.getLogger(BlackListItemService.class);

    public boolean isBlackListed(String personalId) {
        return super.getByPersonalId(personalId)!=null;
    }

    @SuppressWarnings("unchecked")
    public List<BlackListItem> getSortedFullList() {
        List<BlackListItem> list = new ArrayList<>();

        Session session = super.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(BlackListItem.class)
                    .addOrder(Order.asc("personalId"));
            list = criteria.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            log.error("Can't get sorted full list", e);
        } finally {
            session.close();
        }
        log.trace("done, return "+list.size()+" entities");

        return list;
    }
}
