package twinorest.service;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import twinorest.dao.LoanDAOImpl;
import twinorest.entity.Loan;

import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class LoanService extends LoanDAOImpl {
    private Logger log = Logger.getLogger(LoanService.class);

    public final static int DEFAULT_REJECTION_LOANS_COUNT = 1;
    public final static int DEFAULT_REJECTION_TIME_FRAME_DAYS = 3;

    @SuppressWarnings("unchecked")
    public List<Loan> getSortedFullList() {
        List<Loan> list = new ArrayList<>();

        Session session = super.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Loan.class)
                    .addOrder(Order.desc("applyDate"))
                    .addOrder(Order.asc("firstName"))
                    .addOrder(Order.asc("lastName"));
            list = criteria.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            log.error("Can't get sorted full list", e);
        } finally {
            session.close();
        }
        log.trace("done, return "+list.size()+" entities");

        return list;
    }

    @SuppressWarnings("unchecked")
    public List<Loan> getSortedUserList(String personalId) {
        log.trace("Filter apply loans personalId: "+personalId);
        List<Loan> list = new ArrayList<>();

        Session session = super.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Loan.class)
                    .add(Restrictions.eq("personalId", personalId))
                    .addOrder(Order.desc("applyDate"));
            list = criteria.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            log.error("Can't get sorted list of user loans", e);
        } finally {
            session.close();
        }
        log.trace("done, return filtered "+list.size()+" entities");

        return list;
    }

    /**
     * @param countryCode add criteria countryCode
     * @param rejectTimeFrameDays ?>0 filter apply date now - param value
     * @return filtered list (empty list on error)
    */
    @SuppressWarnings("unchecked")
    public List<Loan> getLoansOfCountryList(String countryCode, int rejectTimeFrameDays) {
        log.trace("Filter apply loans countryCode: "+countryCode+", rejectTimeFrameDays: "+ rejectTimeFrameDays);
        List<Loan> list = new ArrayList<>();

        Session session = super.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Loan.class).add(Restrictions.eq("countryCode", countryCode));

            if(rejectTimeFrameDays >0) {
                Calendar cRestrictDatePoint = Calendar.getInstance();
                cRestrictDatePoint.add(Calendar.DAY_OF_MONTH, (rejectTimeFrameDays *-1));
                Date dRestrictDatePoint = new java.sql.Date(cRestrictDatePoint.getTimeInMillis());

                criteria.add(Restrictions.ge("applyDate", dRestrictDatePoint));
            }

            list = criteria.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            log.error("Can't get list of for countryCode: " + countryCode + "", e);
        } finally {
            session.close();
        }
        log.trace("done, return filtered "+list.size()+" entities");

        return list;
    }

    public Loan insert(Loan entity) {
        entity.setApplyDate(new Timestamp((new java.util.Date()).getTime())); //set now date
        entity.setAmount(entity.getAmount().setScale(2, RoundingMode.HALF_EVEN)); //do rounding

        return super.insert(entity);
    }
}
