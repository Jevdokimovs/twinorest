package twinorest.service.helper;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@Service
public class GeoIPChecker {
    private Logger log = Logger.getLogger(GeoIPChecker.class);

    public static final String LANG_CODE_UNDEFINED = "lv"; //take by default lv

    public String getClientIP(HttpServletRequest httpServletRequest) {
        String clientIP = httpServletRequest.getHeader("X-FORWARDED-FOR"); //check if proxy
        if (clientIP == null) {
            clientIP = httpServletRequest.getRemoteAddr();
        }

        return clientIP;
    }

    public String getCountryCode(String ip, Resource rGeoIPdb) {
        String countryCode = LANG_CODE_UNDEFINED;//default set undefined

        try {
            File fGeoIPdb = rGeoIPdb.getFile();

            LookupService cl = new LookupService(fGeoIPdb.getAbsolutePath(), LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);
            Location location = cl.getLocation(ip);
            if (location != null) {
                countryCode = location.countryCode.toLowerCase();
            }
        } catch (IOException e) {
            log.error("Error location via IP ("+ip+")", e);
            return LANG_CODE_UNDEFINED;
        } catch (NullPointerException e) {
            return LANG_CODE_UNDEFINED;
        }

        return countryCode;
    }
}
