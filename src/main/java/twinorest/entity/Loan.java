package twinorest.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

@Entity
@Table(name="loan")
public class Loan {
    private int id;
    private String firstName;
    private String lastName;
    private int term;
    private String personalId;
    private BigDecimal amount;
    private String countryCode;
    private Timestamp applyDate;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name", nullable = false, insertable = true, updatable = true, length = 200)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, insertable = true, updatable = true, length = 200)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "term", nullable = false, insertable = true, updatable = true)
    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    @Basic
    @Column(name = "personal_id", nullable = false, insertable = true, updatable = true, length = 300)
    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    @Basic
    @Column(name = "amount", nullable = false, insertable = true, updatable = true, precision = 12, scale = 2)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "country_code", nullable = false, insertable = true, updatable = true, length = 10)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Basic
    @Column(name = "apply_date", insertable=true, updatable=false)
    public Timestamp getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Timestamp applyDate) {
        this.applyDate = applyDate;
    }

    @Transient
    @SuppressWarnings("unused")
    public String getApplyDateFormatted() {
        if (applyDate == null) {
            return "";
        }
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(applyDate);
    }

    @Transient
    @SuppressWarnings("unused")
    public String getAmountFormatted() {
        if (amount == null) {
            return "0.00";
        }
        return (new DecimalFormat("0.00")).format(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Loan loan = (Loan) o;

        if (id != loan.id) return false;
        if (term != loan.term) return false;
        if (amount != null ? !amount.equals(loan.amount) : loan.amount != null) return false;
        if (applyDate != null ? !applyDate.equals(loan.applyDate) : loan.applyDate != null) return false;
        if (countryCode != null ? !countryCode.equals(loan.countryCode) : loan.countryCode != null) return false;
        if (firstName != null ? !firstName.equals(loan.firstName) : loan.firstName != null) return false;
        if (lastName != null ? !lastName.equals(loan.lastName) : loan.lastName != null) return false;
        if (personalId != null ? !personalId.equals(loan.personalId) : loan.personalId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + term;
        result = 31 * result + (personalId != null ? personalId.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
        result = 31 * result + (applyDate != null ? applyDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id+""+
                ", applyDate=" + (applyDate!=null ? applyDate.toString() : "") +
                ", personalId='" + personalId + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", amount=" + amount +
                ", term=" + term +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}
