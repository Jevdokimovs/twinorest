package twinorest.entity;

import javax.persistence.*;

@Entity
@Table(name="blacklist_item")
public class BlackListItem {
    private int id;
    private String personalId;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "personal_id", unique=true, nullable = false, insertable = true, updatable = false, length = 300)
    public String getPersonalId() {
        return personalId;
    }
    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlackListItem that = (BlackListItem) o;
        if (!personalId.equals(that.personalId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return personalId.hashCode();
    }
}
