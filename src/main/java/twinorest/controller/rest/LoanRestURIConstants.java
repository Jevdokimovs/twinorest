package twinorest.controller.rest;

public class LoanRestURIConstants {
    public static final String CREATE_LOAN = "/loans";
    public static final String GET_ALL_LOANS = "/loans";
    public static final String GET_USER_LOANS = "/loans/user/{personalId}";
    public static final String GET_LOAN = "/loans/{id}";
    public static final String DELETE_LOAN = "/loans/{id}";

    //public static final String ERROR = "/loans/error";
}
