package twinorest.controller.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import twinorest.entity.Loan;
import twinorest.service.BlackListItemService;
import twinorest.service.LoanService;
import twinorest.service.helper.GeoIPChecker;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class LoanController {
    //private Logger log = Logger.getLogger(LoanController.class);

    private LoanService loanService;
    private BlackListItemService blackListItemService;

    @Value(value = "classpath:GeoLiteCity.dat")
    private Resource rGeoIPdb;

    @PostConstruct
    public void init() {
        loanService = new LoanService();
        blackListItemService = new BlackListItemService();
    }

    @RequestMapping(value = LoanRestURIConstants.GET_ALL_LOANS, method = RequestMethod.GET)
    public LoanRestResponse<List<Loan>> loansList() {
        List<Loan> list = loanService.getSortedFullList();

        //noinspection Convert2Diamond
        return new LoanRestResponse<List<Loan>>(list, LoanRestResponse.STATUS_OK, LoanRestResponse.MESSAGES.get(LoanRestResponse.STATUS_OK));
    }

    @RequestMapping(value = LoanRestURIConstants.GET_LOAN, method = RequestMethod.GET)
    public LoanRestResponse<Loan> getLoanById(@PathVariable int id)
    {
        int statusCode = LoanRestResponse.STATUS_OK;
        Loan entity = loanService.getById(id);

        if(entity == null) {
            statusCode = LoanRestResponse.LOAN_NOT_EXISTS;
        }

        //noinspection Convert2Diamond
        return new LoanRestResponse<Loan>(entity, statusCode, LoanRestResponse.MESSAGES.get(statusCode));
    }

    @SuppressWarnings("Convert2Diamond")
    @RequestMapping(value = LoanRestURIConstants.GET_USER_LOANS, method = RequestMethod.GET)
    public LoanRestResponse<List<Loan>> listLoansByUser(@PathVariable String personalId)
    {
        List<Loan> list = loanService.getSortedUserList(personalId);

        return new LoanRestResponse<List<Loan>>(list, LoanRestResponse.STATUS_OK, LoanRestResponse.MESSAGES.get(LoanRestResponse.STATUS_OK));
    }

    @RequestMapping(value = LoanRestURIConstants.CREATE_LOAN, method = RequestMethod.POST)
    public LoanRestResponse<Loan> addLoan(@RequestBody Loan entity, HttpServletRequest httpServletRequest)
    {
        int statusCode = LoanRestResponse.STATUS_OK;

        //validations
        if(entity.getAmount() == null) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_AMOUNT;
        }
        if(statusCode == LoanRestResponse.STATUS_OK && entity.getAmount().signum() < 1) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_AMOUNT;
        }
        if(statusCode == LoanRestResponse.STATUS_OK && entity.getTerm()<1) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_TERM;
        }
        if(statusCode == LoanRestResponse.STATUS_OK && entity.getTerm()<1) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_TERM;
        }
        if(statusCode == LoanRestResponse.STATUS_OK && StringUtils.isEmpty(entity.getFirstName())) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_FIRST_NAME;
        }
        if(statusCode == LoanRestResponse.STATUS_OK && StringUtils.isEmpty(entity.getLastName())) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_LAST_NAME;
        }
        if(statusCode == LoanRestResponse.STATUS_OK && StringUtils.isEmpty(entity.getPersonalId())) {
            statusCode = LoanRestResponse.VALIDATION_FAILED_PERSONAL_ID;
        }
        if(statusCode == LoanRestResponse.STATUS_OK) {
            GeoIPChecker geoIPChecker = new GeoIPChecker();
            String clientIP = geoIPChecker.getClientIP(httpServletRequest);

            String countryCode = geoIPChecker.getCountryCode(clientIP, rGeoIPdb);
            entity.setCountryCode(countryCode);

           //limit in time frame(now - n days)
            List<Loan> rejectCheckList = loanService.getLoansOfCountryList(entity.getCountryCode(), LoanService.DEFAULT_REJECTION_TIME_FRAME_DAYS);
            if(rejectCheckList.size()>=LoanService.DEFAULT_REJECTION_LOANS_COUNT) {
                statusCode = LoanRestResponse.REJECTION_APPLICATIONS_LIMIT;
            }
        }
        if(statusCode == LoanRestResponse.STATUS_OK && blackListItemService.isBlackListed(entity.getPersonalId())) {
            statusCode = LoanRestResponse.REJECTION_PERSONAL_ID_BLACKLISTED;
        }

        if(statusCode == LoanRestResponse.STATUS_OK) {
            entity = loanService.insert(entity);
            if(entity == null) {
                statusCode = LoanRestResponse.ADD_FAILED;
            }
        }

        //noinspection Convert2Diamond
        return new LoanRestResponse<Loan>(entity, statusCode, LoanRestResponse.MESSAGES.get(statusCode));
    }

    @RequestMapping(value = LoanRestURIConstants.DELETE_LOAN, method = RequestMethod.DELETE)
    public LoanRestResponse<Loan> deleteLoan(@PathVariable int id)
    {
        int statusCode = LoanRestResponse.STATUS_OK;
        Loan entity = loanService.getById(id);

        if(entity == null) {
            statusCode = LoanRestResponse.LOAN_NOT_EXISTS;
        }
        if(statusCode == LoanRestResponse.STATUS_OK) {
            if(!loanService.delete(entity)){
                statusCode = LoanRestResponse.DELETE_FAILED;
            }
        }

        //noinspection Convert2Diamond
        return new LoanRestResponse<Loan>(entity, statusCode, LoanRestResponse.MESSAGES.get(statusCode));
    }

    //parse request error
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public LoanRestResponse handleHttpMessageNotReadableException() {
        int statusCode = LoanRestResponse.INVALID_REQUEST;

         return new LoanRestResponse(statusCode, LoanRestResponse.MESSAGES.get(statusCode));
    }
}
