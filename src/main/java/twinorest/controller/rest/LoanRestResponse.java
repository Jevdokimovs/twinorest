package twinorest.controller.rest;

import java.util.HashMap;
import java.util.Map;

public class LoanRestResponse<T> {
    public static final int STATUS_OK = 0;

    public static final int INVALID_REQUEST = 1;
    public static final int LOAN_NOT_EXISTS = 2;

    public static final int DELETE_FAILED = 3;
    public static final int ADD_FAILED = 4;

    public static final int VALIDATION_FAILED_AMOUNT = 5;
    public static final int VALIDATION_FAILED_TERM = 6;
    public static final int VALIDATION_FAILED_FIRST_NAME = 7;
    public static final int VALIDATION_FAILED_LAST_NAME = 8;
    public static final int VALIDATION_FAILED_PERSONAL_ID = 9;

    final static int REJECTION_APPLICATIONS_LIMIT = 10;
    final static int REJECTION_PERSONAL_ID_BLACKLISTED = 11;

    final static Map<Integer, String> MESSAGES = new HashMap<Integer, String>() {{
        put(STATUS_OK, "Status ok");
        put(INVALID_REQUEST, "Error! Invalid request data");
        put(LOAN_NOT_EXISTS, "Error! Loan not found");
        put(DELETE_FAILED, "Error! Delete failed");
        put(ADD_FAILED, "Error! Loan add failed");
        put(VALIDATION_FAILED_AMOUNT, "Validation failed, loan amount incorrect");
        put(VALIDATION_FAILED_TERM, "Validation failed, loan term incorrect");
        put(VALIDATION_FAILED_FIRST_NAME, "Validation failed, first name incorrect");
        put(VALIDATION_FAILED_LAST_NAME, "Validation failed, last name incorrect");
        put(VALIDATION_FAILED_PERSONAL_ID, "Validation failed, personal ID incorrect");
        put(REJECTION_APPLICATIONS_LIMIT, "Loan application rejected. Applications limit reached.");
        put(REJECTION_PERSONAL_ID_BLACKLISTED, "Loan application rejected. Person blacklisted.");
    }};

    private T data;
    private boolean success;
    private int statusCode;
    private String message;

    @SuppressWarnings("unused")
    public LoanRestResponse() {}

    public LoanRestResponse(T data, int statusCode, String message) {
        this.data = data;
        this.statusCode = statusCode;
        this.success = (statusCode == STATUS_OK);
        this.message = message;
    }

    public LoanRestResponse(int statusCode, String message) {
        this.data = null;
        this.statusCode = statusCode;
        this.success = (statusCode == STATUS_OK);
        this.message = message;
    }

    @SuppressWarnings("unused")
    public T getData() {
        return data;
    }

    @SuppressWarnings("unused")
    public void setData(T data) {
        this.data = data;
    }

    @SuppressWarnings("unused")
    public boolean isSuccess() {
        return success;
    }

    @SuppressWarnings("unused")
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @SuppressWarnings("unused")
    public int getStatusCode() {
        return statusCode;
    }

    @SuppressWarnings("unused")
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @SuppressWarnings("unused")
    public String getMessage() {
        return message;
    }

    @SuppressWarnings("unused")
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "LoanRestResponse{" +
            "data=" + data +
            ", success=" + success +
            ", statusCode=" + statusCode +
            ", message='" + message + '\'' +
            '}';
    }
}
