package twinorest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import twinorest.entity.BlackListItem;
import twinorest.service.BlackListItemService;

import javax.annotation.PostConstruct;
import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {
    //private Logger log = Logger.getLogger(IndexController.class);
    private BlackListItemService blackListItemService;

    @PostConstruct
    public void init() {
        blackListItemService = new BlackListItemService();
    }

    @RequestMapping(value = "/ajax/get-blacklist/", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody
    List<BlackListItem> blackListItemList(
    ) {
        return blackListItemService.getSortedFullList();
    }
}
