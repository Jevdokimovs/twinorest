/*Fill with test data*/
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (2, 12000.00, '2016-09-01 15:57:54', 'uk', 'Bobby', 'Hunt', '250195-34562', 36);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (3, 1000.00, '2015-10-01 12:01:07', 'uk', 'Craig', 'Dixon', '310576-10875', 12);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (4, 55000.00, '2016-02-01 16:03:55', 'us', 'Steven', 'Fisher', '250191-98765', 96);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (5, 25000.00, '2016-08-05 16:04:23', 'lv', 'Jānis', 'Berziņš', '301181-10325', 24);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (6, 18000.50, '2016-07-15 16:05:59', 'lv', 'Māris', 'Kalniņš', '241275-35865', 24);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (7, 36000.00, '2016-02-04 16:05:59', 'ru', 'Владислав', 'Миронов', '120387-45332', 48);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (8, 1500.75, '2014-02-01 16:12:34', 'ru', 'Дмитрий', 'Егоров', '030275-43352', 6);
INSERT INTO twinotest.loan (id, amount, apply_date, country_code, first_name, last_name, personal_id, term) VALUES (9, 1000.00, '2015-03-07 16:15:29', 'ua', 'Егор', 'Привалов', '250168-43567', 6);

INSERT INTO twinotest.blacklist_item (id, personal_id) VALUES (1, '030275-43352');
INSERT INTO twinotest.blacklist_item (id, personal_id) VALUES (2, '241275-35865');

