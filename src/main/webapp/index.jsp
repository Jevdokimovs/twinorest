<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Test REST</title>
        <meta charset="utf-8" />

        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.80/jsrender.js" type="text/javascript"></script>

        <script id="loanRow-tmpl" type="text/html">
            <tr data-id="{{>id}}" class="loanRow">
                <td>{{>applyDateFormatted}}</td>
                <td>{{>personalId}}</td>
                <td>{{>countryCode}}</td>
                <td>{{>firstName}}</td>
                <td>{{>lastName}}</td>
                <td>{{>amountFormatted}} &euro;</td>
                <td>{{>term}}</td>
                <td><button class="deleteLoanButton" type="button" data-id="{{>id}}">delete</button></td>
            </tr>
        </script>

        <script id="blacklistRow-tmpl" type="text/html">
            <li data-id="{{>id}}">{{>personalId}}</li>
        </script>

        <script type="text/javascript">
            var restURL = '/loans/';

            $(document).ready(function() {
                //blacklist
                $.ajax({
                    type: 'GET',
                    url:  "/ajax/get-blacklist/",
                    dataType: 'json',
                    async: true,
                    success: function(data) {
                        if(data!=null) {
                            $("#blacklist").html($("#blacklistRow-tmpl").render(data));
                        } else {
                            $("#msgWrap").html(response.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {}
                });

                //load loans
                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    type: 'GET',
                    url:  restURL,
                    dataType: 'json',
                    async: true,
                    success: function(response) {
                         if(response.success) {
                            $("#loanRows").html($("#loanRow-tmpl").render(response.data));
                        } else {
                            $("#msgWrap").html(response.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {}
                });

                //add form post
                $('#createLoanForm').submit(function(e) {
                    e.preventDefault();

                    var json = convertFormToJSON($(this));
                    $("#msgWrap").empty();
                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        type: 'POST',
                        url:  restURL,
                        dataType: 'json',
                        data: JSON.stringify(json),
                        async: true,
                        success: function(response) {
                            if(response.success) {
                                $("#loanRows").prepend($("#loanRow-tmpl").render(response.data));
                                $('#createLoanForm').trigger("reset");
                            } else {
                                $("#msgWrap").html(response.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {}
                    });
                });

                //delete
                $('#loansList').on('click', '.deleteLoanButton', function(e) {
                    var id = $(this).data("id");
                    var loanRow = $(this).closest('.loanRow');
                    $("#msgWrap").empty();

                    $.ajax({
                        type: 'DELETE',
                        url:  restURL+id,
                        success: function(response) {
                            if(response.success) {
                                loanRow.remove();
                            } else {
                                $("#msgWrap").html(response.message);
                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {}
                    });
                });
            });

            //helpers
            function convertFormToJSON(form){
                var array = jQuery(form).serializeArray();
                var json = {};

                jQuery.each(array, function() {
                    json[this.name] = this.value || '';
                });

                return json;
            }
        </script>

        <style>
            #loansList td, #loansList th {border: 1px solid whitesmoke; padding: 5px;}
            .block {float: left; margin: 10px;}

            #blacklist {list-style: none; color: red; font-size: 0.9em;}
        </style>
    </head>

    <body>
            <div id="msgWrap"></div>
            <span class="block" id="listWrap">
                <table id="loansList">
                    <caption>Loan applications</caption>
                    <thead>
                        <tr>
                            <th>Apply Date</th>
                            <th>Personal ID</th>
                            <th>Country</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Amount<br>EUR</th>
                            <th>Term<br>month</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="loanRows"></tbody>
                </table>
            </span>
            <span class="block" id="formWrap">
                <form id="createLoanForm">
                    <table>
                        <caption>Apply form</caption>
                        <tr><td><label for="amount">Loan amount:</label></td><td><input type="text" name="amount" id="amount"></td></tr>
                        <tr><td><label for="term">Term(month):</label></td><td><input type="text" name="term" id="term"></td></tr>
                        <tr><td><label for="firstName">First name:</label></td><td><input type="text" name="firstName" id="firstName"></td></tr>
                        <tr><td><label for="lastName">Last name:</label></td><td><input type="text" name="lastName" id="lastName"></td></tr>
                        <tr><td><label for="personalId">Personal id:</label></td><td><input type="text" name="personalId" id="personalId"></td></tr>
                        <tr><td colspan="2" align="right"><button type="submit">Apply</button></td></tr>
                    </table>
                </form>
            </span>
            <span class="block" id="blackListWrap">
                Blacklisted personal IDs:
                <ul id="blacklist"></ul>
            </span>
    </body>
</html>